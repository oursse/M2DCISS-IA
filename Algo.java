package proto.copy4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class Algo {

	Grid grid;
	int size;
	int tick;
	String results;
	public boolean delay = false;

	public Algo(Grid grid) {
		this.grid = grid;
		this.size = 63 * 16 / Game.size;
		this.tick = (this.size > 50) ? (this.size > 100 ? 0 : (201 / this.size)) : (250 / this.size);
		System.out.println(this.tick);
	}

	public void dfs() {
		this.grid.reset();
		long timer = System.currentTimeMillis();
		int count = 0;

		Stack<Node> stack = new Stack<Node>();
		stack.push(this.grid.start);

		while (!stack.isEmpty()) {
			Node current = stack.pop();
			sleepy(!delay ? -1 : tick);
			if (current.type == Type.Explored) {
				continue;
			}

			if (current.equals(this.grid.end)) {
				this.results = perf2(timer, "Depth-First-Search", count, traceback(current));
				break;
			}

			current.type = Type.Current;
			Node[] children = grid.getNeighbours(current);

			for (Node child : children) {
				if (child == null || stack.contains(child) || child.type == Type.Wall || child.type == Type.Start
						|| child.type == Type.Explored) {
					continue;
				}

				if (child.type != Type.Target) {
					child.type = Type.Open;
				}

				child.parent = current;

				stack.push(child);
			}
			if (current != this.grid.start) {
				current.type = Type.Explored;
			} else {
				current.type = Type.Start;
			}
			count++;
		}
	}

	public void bfs() {
		this.grid.reset();
		long timer = System.currentTimeMillis();
		int count = 0;

		Queue<Node> queue = new LinkedList<Node>();
		queue.add(grid.start);

		while (!queue.isEmpty()) {
			Node current = queue.remove();
			sleepy(!delay ? -1 : tick);
			if (current.equals(this.grid.end)) {
				this.results = perf2(timer, "Breadth-First-Search", count, traceback(current));
				break;
			}

			current.type = Type.Current;
			Node[] voisins = grid.getNeighbours(current);

			for (int i = 0; i < 4; i++) {

				if (voisins[i] == null || (voisins[i].type == Type.Wall || voisins[i].type == Type.Start
						|| voisins[i].type == Type.Explored)) {
					continue;
				}

				if (!queue.contains(voisins[i])) {
					voisins[i].parent = current;
					if (voisins[i].type != Type.Target) {
						voisins[i].type = Type.Open;
					}
					queue.add(voisins[i]);
				}
			}
			count++;
			if (current != this.grid.start) {
				current.type = Type.Explored;
			} else {
				current.type = Type.Start;
			}
		}
	}

	/**
	 * The only difference with bfs is the use of PriorityQueue with an heuristic
	 * Here, the heuristic used is Manathan
	 */
	public void gbfs() {
		this.grid.reset();
		long timer = System.currentTimeMillis();
		int count = 0;

		class nodeComparator implements Comparator<Node> {
			@Override
			public int compare(Node o1, Node o2) {
				return o1.h - o2.h;
			}
		}
		;

		PriorityQueue<Node> q = new PriorityQueue<Node>(new nodeComparator());
		q.add(grid.start);

		while (!q.isEmpty()) {
			Node current = q.remove();
			sleepy(!delay ? -1 : tick);
			if (current.equals(this.grid.end)) {
				this.results = perf2(timer, "Greedy-Breadth-First-Search", count, traceback(current));
				break;
			}

			current.type = Type.Current;
			Node[] voisins;
			voisins = grid.getNeighbours(current);

			for (Node node : voisins) {
				if (node == null || node.type == Type.Wall || node.type == Type.Start || node.type == Type.Explored) {
					continue;
				}

				if (!q.contains(node)) {
					node.parent = current;

					if (node.type != Type.Target) {
						node.type = Type.Open;
					}

					node.h = manathan(node, this.grid.end);

					q.add(node);
				}
			}
			count++;
			if (current != this.grid.start) {
				current.type = Type.Explored;
			} else {
				current.type = Type.Start;
			}
		}
	}

	public void dijkstra() {
		this.grid.reset();
	}

	public void astar(boolean manhat) {
		this.grid.reset();
		long timer = System.currentTimeMillis();
		int count = 0;

		class nodeComparator implements Comparator<Node> {
			@Override
			public int compare(Node o1, Node o2) {
				return o1.f - o2.f;
			}
		}
		;

		ArrayList<Node> open = new ArrayList<Node>();
		open.add(this.grid.start);
		while (!open.isEmpty()) {
			Collections.sort(open, new nodeComparator());
			Node current = open.remove(0);
			sleepy(!delay ? -1 : tick);
			if (current.equals(this.grid.end)) {
				String name = manhat ? "Manhattan" : "Euclidian";
				this.results = perf2(timer, "A Star (" + name + " heuristic" + ")", count, traceback(current));
				break;
			}

			current.type = Type.Current;
			Node[] children;
			if (manhat) {
				children = grid.getNeighbours(current);
			} else {
				children = grid.getNeighbours8(current);
			}

			for (Node child : children) {
				if (child == null || child.type == Type.Explored || child.type == Type.Wall
						|| child.type == Type.Start) {
					continue;
				}

				if (child.type != Type.Target) {
					child.type = Type.Open;
				}

				child.parent = current;
				child.g = current.g + 1;

				if (manhat) {
					child.h = manathan(child, this.grid.end);
				} else {
					child.h = euclidian(child, this.grid.end);
				}

				child.f = child.g + child.h;

				if (open.contains(child)) {
					if (child.g >= open.get(open.indexOf(child)).g) {
						continue;
					}
				}
				open.add(child);
			}
			if (current != this.grid.start) {
				current.type = Type.Explored;
			} else {
				current.type = Type.Start;
			}
			count++;
		}

	}

	private void perf(long pt, String name, int c, int p) {
		System.out.println("		ALGORITHME " + name);
		System.out.println("#	Temps (ms) : " + ((System.currentTimeMillis() - pt) - c * this.tick));
		System.out.println("#	Temps r�el (ms) : " + (System.currentTimeMillis() - pt));
		System.out.println("#	Nombre de noeuds explor� : " + c);
		System.out.println("#	Nombre de noeuds dans le path final : " + p);
		System.out.println();
	}

	private String perf2(long pt, String name, int c, int p) {
		String ls = System.lineSeparator();
		String rtn = "## ALGORITHME " + name + ls;
		rtn += delay ? "" : "# Temps r�el (ms) : " + (System.currentTimeMillis() - pt) + ls;
		rtn += "# Nombre de noeuds explor� : " + c + ls;
		rtn += "# Nombre de noeuds dans le path final : " + p + ls;
		return rtn;
	}

	private int traceback(Node n) {
		if (n.type != Type.Start && n.parent != null) {
			if (n.type != Type.Target) {
				n.type = Type.Final;
			}
			return 1 + traceback(n.parent);
		} else if (n.type == Type.Start) {
			return 1;
		}
		return 0;
	}

	private void sleepy(int s) {
		if (s == -1) {
			return;
		}
		
		try {
			if (s == 0) {
				Thread.sleep(0,1);
			} else {
				Thread.sleep(s);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void test() {

	}

	public int manathan(Node n, Node t) {
		return Math.abs(n.coord.x - t.coord.x) + Math.abs(n.coord.y - t.coord.y);
	}

	public int euclidian(Node n, Node t) {
		return (int) (Math.pow(n.coord.x - t.coord.x, 2) + Math.pow(n.coord.y - t.coord.y, 2));
	}
}
