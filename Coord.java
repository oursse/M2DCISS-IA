package proto.copy4;

public class Coord {
	public int x, y;

	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
