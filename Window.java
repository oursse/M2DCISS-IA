package proto.copy4;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Window {

	Algo algoW;

	public Window(int width, int height, String title, Game game, Grid grid) {

		this.algoW = new Algo(grid);

		JFrame frame = new JFrame(title);
		int w = width + 300;

		Font fnt = new Font("arial", 1, 35);
		Font fntlt = new Font("arial", 4, 20);
		Font fntvlt = new Font("arial", 4, 12);

		JPanel jp = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Dimension getPreferredSize() {
				return new Dimension(300, height);
			};
		};
		jp.setLayout(null);
		jp.setSize(300, height);
		jp.setLocation(width - 15, 0);

		Label l = new Label("Pathfinding 2.2");
		l.setAlignment(Label.CENTER);
		l.setBounds(0, 0, 300, 100);
		l.setFont(fnt);
		jp.add(l);

		TextArea area = new TextArea("");
		area.setBounds(1, 350, 295, 650);
		area.setEditable(false);
		jp.add(area);
		
		Checkbox dly = new Checkbox("Delay");
		dly.setFont(fntlt);
		dly.setBounds(20, 145, 80, 50);
		dly.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				algoW.delay = !algoW.delay; 
			}
		});
		jp.add(dly);

		Button r = new Button("Random");
		r.setFont(fntlt);
		r.setBounds(110, 155, 80, 30);
		r.addActionListener(new ap(grid, algoW, area));
		jp.add(r);

		Button rst = new Button("Reset");
		rst.setFont(fntlt);
		rst.setBounds(205, 155, 80, 30);
		rst.addActionListener(new ap(grid, algoW, area));
		jp.add(rst);

		Button t1 = new Button("Test 1");
		t1.setFont(fntlt);
		t1.setBounds(15, 110, 80, 30);
		t1.addActionListener(new ap(grid, algoW, area));
		jp.add(t1);

		Button t2 = new Button("Test 2");
		t2.setFont(fntlt);
		t2.setBounds(110, 110, 80, 30);
		t2.addActionListener(new ap(grid, algoW, area));
		jp.add(t2);

		Button t3 = new Button("Test 3");
		t3.setFont(fntlt);
		t3.setBounds(205, 110, 80, 30);
		t3.addActionListener(new ap(grid, algoW, area));
		jp.add(t3);

		Label taille = new Label("Taille de la grille :");
		taille.setAlignment(Label.CENTER);
		taille.setBounds(15, 212, 100, 20);
		taille.setFont(fntvlt);
		jp.add(taille);

		final Choice c = new Choice();
		c.setBounds(125, 210, 75, 75);
		c.add("9");
		c.add("15");
		c.add("33");
		c.add("51");
		c.add("66");
		c.add("101");
		c.add("151");
		c.add("201");
		c.select(Integer.toString(Game.gridS));
		jp.setLayout(null);
		jp.setVisible(true);
		jp.add(c);

		Button b = new Button("Valider");
		b.setBounds(225, 210, 50, 20);
		b.addActionListener(new apg(grid, c, game, frame));
		jp.add(b);

		Button dfs = new Button("DFS");
		dfs.setFont(fntlt);
		dfs.setBounds(15, 250, 80, 30);
		dfs.addActionListener(new ap(grid, algoW, area));
		jp.add(dfs);

		Button bfs = new Button("BFS");
		bfs.setFont(fntlt);
		bfs.setBounds(110, 250, 80, 30);
		bfs.addActionListener(new ap(grid, algoW, area));
		jp.add(bfs);

		Button gbfs = new Button("GBFS");
		gbfs.setFont(fntlt);
		gbfs.setBounds(205, 250, 80, 30);
		gbfs.addActionListener(new ap(grid, algoW, area));
		jp.add(gbfs);

		Button asm = new Button("ASTAR-M");
		asm.setFont(fntlt);
		asm.setBounds(15, 300, 125, 30);
		asm.addActionListener(new ap(grid, algoW, area));
		jp.add(asm);

		Button ase = new Button("ASTAR-E");
		ase.setFont(fntlt);
		ase.setBounds(160, 300, 125, 30);
		ase.addActionListener(new ap(grid, algoW, area));
		jp.add(ase);

//		Panel disp = new Panel();
//		JTextPane txt = new JTextPane();
//		disp.add( new JScrollPane( txt ) );
//		MessageConsole mc = new MessageConsole(textComponent);
//		mc.redirectOut();
//		mc.redirectErr(Color.RED, null);
//		mc.setMessageLines(100);

//		Button dfs = new Button("DFS");
//		dfs.setFont(fntlt);
//		dfs.setBounds(15, 230, 80, 30);
//		dfs.addActionListener(new ap(grid));
//		jp.add(dfs);

		frame.add(jp);

		frame.setPreferredSize(new Dimension(w, height));
		frame.setMaximumSize(new Dimension(w, height));
		frame.setMinimumSize(new Dimension(w, height));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.add(game);
		frame.setVisible(true);
		game.start();
	}

	class ap implements ActionListener {

		Grid grid;
		Algo algo;
		TextArea area;

		public ap(Grid grid, Algo algo, TextArea area) {
			this.grid = grid;
			this.algo = algo;
			this.area = area;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equalsIgnoreCase("Random")) {
				this.grid.init(0);
			} else if (e.getActionCommand().equalsIgnoreCase("Reset")) {
				this.grid.reset();
			} else if (e.getActionCommand().equalsIgnoreCase("Test 1")) {
				this.grid.init(1);
			} else if (e.getActionCommand().equalsIgnoreCase("Test 2")) {
				this.grid.init(2);
			} else if (e.getActionCommand().equalsIgnoreCase("Test 3")) {
				this.grid.init(3);
			} else if (e.getActionCommand().equalsIgnoreCase("DFS")) {
				this.algo.dfs();
				area.setText(algo.results + System.lineSeparator() + area.getText());
			} else if (e.getActionCommand().equalsIgnoreCase("BFS")) {
				this.algo.bfs();
				area.setText(algo.results + System.lineSeparator() + area.getText());
			} else if (e.getActionCommand().equalsIgnoreCase("GBFS")) {
				this.algo.gbfs();
				area.setText(algo.results + System.lineSeparator() + area.getText());
			} else if (e.getActionCommand().equalsIgnoreCase("ASTAR-M")) {
				this.algo.astar(true);
				area.setText(algo.results + System.lineSeparator() + area.getText());
			} else if (e.getActionCommand().equalsIgnoreCase("ASTAR-E")) {
				this.algo.astar(false);
				area.setText(algo.results + System.lineSeparator() + area.getText());
			}
		}
	}

	class apg implements ActionListener {

		Grid grid;
		Choice c;
		Game game;
		JFrame frame;

		public apg(Grid grid, Choice c, Game game, JFrame frame) {
			this.grid = grid;
			this.c = c;
			this.game = game;
			this.frame = frame;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			switch (c.getItem(c.getSelectedIndex())) {
			case "9":
				new Game(9);
				frame.dispose();
				break;
			case "15":
				new Game(15);
				frame.dispose();
				break;
			case "33":
				new Game(33);
				frame.dispose();
				break;
			case "51":
				new Game(51);
				frame.dispose();
				break;
			case "66":
				new Game(66);
				frame.dispose();
				break;
			case "101":
				new Game(101);
				frame.dispose();
				break;
			case "151":
				new Game(151);
				frame.dispose();
				break;
			case "201":
				new Game(201);
				frame.dispose();
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + c.getItem(c.getSelectedIndex()));
			}

		}

	}
}
