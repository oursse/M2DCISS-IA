# M2DCISS-IA

# Version 2.2 : Presentation version

## Commands :

###### General :

- Esc to quit
- F5 to generate a new random map
- F4 to clear the grid
- 1 to setup Test A
- 2 to setup Test B
- 3 to setup Test C

###### Algorithm :

- A to run the **A*** algorithm (<u><strong>manathan</strong></u> heuristic)
- Z to run the **A*** algorithm (<u><strong>euclidian</strong></u> heuristic)
- G to run the **GreedyBreadthFirstSearch** algorithm (manathan heuristic)
- B to run the **BreadthFirstSearch** algorithm
- D to run **DepthFirstSearch** algorithm

## Changelog

- Improvements of the GUI

## Inspiration and learning materials

##### Amit Patel's site :

https://www.redblobgames.com/pathfinding/a-star/introduction.html

##### Sebastian Lague's youtube channel and github :

https://www.youtube.com/watch?v=-L-WgKMFuhE

https://github.com/SebLague/Pathfinding
