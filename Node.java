package proto.copy4;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;

public class Node extends MouseAdapter {
	public Coord coord;
	public Type type;
	public Node parent = null;

	public int g = 0;
	public int h = 0;
	public int f = 0;

	protected Color color;

	public Node(int x, int y, Type type) {
		this.coord = new Coord(x, y);
		this.type = type;
	}

	// GRAPHICS

	public String info() {
		return "x:" + parent.coord.x + " y:" + parent.coord.y;
	}

	/**
	 * Return an arrow showing where is the parent node Works only with manhattan
	 * heuristic
	 * 
	 * @return
	 */
	public String parent() {
		String sign = "";
		if (parent != null) {
			if (coord.x == parent.coord.x) {
				if (coord.y > parent.coord.y) {
					sign = sign + "/\\";
				} else {
					sign = sign + "\\/";
				}
			} else {
				if (coord.x > parent.coord.x) {
					sign = sign + "<";
				} else {
					sign = sign + ">";
				}
			}
		}
		return sign;
	}

	public void tick() {
		if (this.type == Type.Wall)
			color = Color.red;
		else if (this.type == Type.Open)
			color = Color.yellow;
		else if (this.type == Type.Explored)
			color = Color.DARK_GRAY;
		else if (this.type == Type.Start)
			color = Color.blue;
		else if (this.type == Type.Target)
			color = Color.green;
		else if (this.type == Type.Current)
			color = Color.WHITE;
		else if (this.type == Type.Final)
			color = Color.magenta;
		else
			color = Color.LIGHT_GRAY;
	}

	public void render(Graphics g) {
		g.setColor(color);
		g.fillRect(coord.x * Game.size, coord.y * Game.size, Game.size, Game.size);
//		g.setColor(Color.black);
//		g.drawString("G:" + this.g + " | H:" + this.h + " | F:" + this.f, coord.x * Game.size + 0,
//				coord.y * Game.size + 60);
////		g.drawString("x:"+this.coord.x+" y:"+this.coord.y, coord.x * Game.size + 10, coord.y * Game.size + 20);
//		if (parent != null) {
//			g.setColor(Color.white);
//			g.drawString(info(), coord.x * Game.size + 10, coord.y * Game.size + 20);
//			g.drawString(parent(), coord.x * Game.size + 30, coord.y * Game.size + 50);
//		}
		g.setColor(Color.black);
		g.drawRect(coord.x * Game.size, coord.y * Game.size, Game.size, Game.size);
	}
}
