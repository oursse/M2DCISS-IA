package proto.copy4;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import proto.copy4.Window;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = -699858510232380730L;

	private int WIDTH;
	private int HEIGHT;

	private Thread thread;
	private boolean running = false;
	private Grid grid;

	static int size;
	static int gridS;

	public Game(int gridSize) {
		Game.gridS = gridSize;
		Game.size = 63 * 16 / gridSize;
		this.grid = new Grid(gridSize);
		this.addKeyListener(new KeyInput(grid));
		this.addMouseListener(new MouseInput(grid));
		this.WIDTH = gridSize * size + 16;
		this.HEIGHT = gridSize * size + 40;

		new Window(WIDTH, HEIGHT, "PathFinding 2.2", this, this.grid);
	}

	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}

	public synchronized void stop() {
		try {
			thread.join();
			running = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				delta--;
			}
			if (running)
				render();

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
			}
		}
		stop();
	}

	private void tick() {
		grid.tick();
	}

	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();

		g.setColor(Color.white);
		g.fillRect(0, 0, WIDTH, HEIGHT);

		grid.render(g);

		g.dispose();
		bs.show();
	}

	public static void main(String[] args) {
		new Game(9);
	}
}
