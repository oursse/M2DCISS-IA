package proto.copy4;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseInput extends MouseAdapter {

	private Grid gryd;

	public MouseInput(Grid gryd) {
		this.gryd = gryd;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int mx = e.getX();
		int my = e.getY();

		for (int x = 0; x < this.gryd.grid.length; x++) {
			for (int y = 0; y < this.gryd.grid.length; y++) {
				if (mouseOver(mx, my, this.gryd.grid[x][y].coord.x * Game.size,
						this.gryd.grid[x][y].coord.y * Game.size, Game.size, Game.size)) {
					if (this.gryd.grid[x][y].type == Type.Empty) {
						this.gryd.grid[x][y].type = Type.Wall;
					} else if (this.gryd.grid[x][y].type == Type.Wall) {
						this.gryd.grid[x][y].type = Type.Empty;
					}
				}
			}
		}
	}

	private boolean mouseOver(int mx, int my, int x, int y, int width, int height) {
		if (mx > x && mx < x + width) {
			if (my > y && my < y + height) {
				return true;
			} else
				return false;
		} else
			return false;
	}

}
