package proto.copy4;

import java.awt.Graphics;
import java.util.Random;

public class Grid {
	Node[][] grid;
	Node start;
	Node end;
	Random r = new Random();

	public Grid(int sizeG) {
		grid = new Node[sizeG][sizeG];
		init(0);
	}

	// GRID BUILDING

	/**
	 * Fill the grid with Empty Nodes
	 * Then, depending on the mode, set start and end
	 * Finally setWall
	 */
	public void init(int mode) {
		for (int x = 0; x < grid.length; x++) {
			for (int y = 0; y < grid.length; y++) {
				grid[x][y] = new Node(x, y, Type.Empty);
			}
		}

		if (mode == 0) {
			setStartAndEndRandom();
		} else if (mode == 1) {
			setStartAndEnd(0, 0, this.grid.length - 1, this.grid.length - 1);
		} else if (mode == 2) {
			setStartAndEnd((this.grid.length - 1) / 2, 0, (this.grid.length - 1) / 2, (this.grid.length - 1));
		} else if (mode == 3) {
			setStartAndEnd(this.grid.length - 1, 0, 0, this.grid.length - 1);
		}
		setWall();

	}

	/**
	 * Turn the grid back to before the execution of an algorithm
	 */
	public void reset() {
		for (int x = 0; x < grid.length; x++) {
			for (int y = 0; y < grid.length; y++) {
				if (!(grid[x][y].type == Type.Target || grid[x][y].type == Type.Wall
						|| grid[x][y].type == Type.Start)) {
					grid[x][y] = new Node(x, y, Type.Empty);
				}
			}
		}
	}

	private void setStartAndEnd(int sx, int sy, int ex, int ey) {
		start = grid[sx][sy] = new Node(sx, sy, Type.Start);
		end = grid[ex][ey] = new Node(ex, ey, Type.Target);
	}

	private void setStartAndEndRandom() {
		int r1 = r.nextInt(grid.length), r2 = r.nextInt(grid.length);
		setStartAndEnd(r1, 0, r2, grid.length - 1);
	}

	/**
	 * Set walls randomly except if wall is on start, end or Von Neumann neighbours
	 */
	private void setWall() {
		for (int i = 0; i < grid.length * grid.length / 3; i++) {
			int x = r.nextInt(grid.length);
			int y = r.nextInt(grid.length);
			Node w = new Node(x, y, Type.Wall);
			boolean ok = true;

			Node[] s = getNeighbours(start);
			for (Node node : s) {
				if (node == null) {
					continue;
				}
				if ((node.coord.x == w.coord.x) && (node.coord.y == w.coord.y)) {
					ok = false;
				}
			}

			Node[] e = getNeighbours(end);
			for (Node node : e) {
				if (node == null) {
					continue;
				}
				if ((node.coord.x == w.coord.x) && (node.coord.y == w.coord.y)) {
					ok = false;
				}
			}

			if ((start.coord.x == w.coord.x) && (start.coord.y == w.coord.y)) {
				ok = false;
			} else if ((end.coord.x == w.coord.x) && (end.coord.y == w.coord.y)) {
				ok = false;
			}

			if (ok) {
				grid[x][y] = w;
			}

		}
	}

	// GETTERS

	public Node getNode(int x, int y) {
		try {
			return grid[x][y];
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Return an array of the neighbours' nodes of current
	 * 
	 * @param current : the Node you want the neighbours from
	 * @return [W,N,E,S] null if out of grid
	 */
	Node[] getNeighbours(Node current) {
		Node[] neighbours = new Node[4];
		neighbours[0] = this.getNode(current.coord.x - 1, current.coord.y); // W
		neighbours[1] = this.getNode(current.coord.x, current.coord.y - 1); // N
		neighbours[2] = this.getNode(current.coord.x + 1, current.coord.y); // E
		neighbours[3] = this.getNode(current.coord.x, current.coord.y + 1); // S
		return neighbours;
	}

	/**
	 * Return an array of the neighbours' nodes of current
	 * 
	 * @param current
	 * @return [N,NW,W,SW,S,SE,E,NE] null if out of grid
	 */
	Node[] getNeighbours8(Node current) {
		Node[] neighbours = new Node[8];
		neighbours[0] = this.getNode(current.coord.x - 1, current.coord.y); // W
		neighbours[1] = this.getNode(current.coord.x - 1, current.coord.y - 1); // NW
		neighbours[2] = this.getNode(current.coord.x, current.coord.y - 1); // N
		neighbours[3] = this.getNode(current.coord.x + 1, current.coord.y - 1);// NE
		neighbours[4] = this.getNode(current.coord.x + 1, current.coord.y); // E
		neighbours[5] = this.getNode(current.coord.x + 1, current.coord.y + 1); // SE
		neighbours[6] = this.getNode(current.coord.x, current.coord.y + 1); // S
		neighbours[7] = this.getNode(current.coord.x - 1, current.coord.y + 1); // SW

		return neighbours;
	}

	// GRAPHICS

	public void tick() {
		int i, j;
		for (i = 0; i < grid.length; i++) {
			for (j = 0; j < grid[i].length; j++) {
				Node tempCase = grid[i][j];
				tempCase.tick();
			}
		}
	}

	public void render(Graphics g) {
		int i, j;
		for (i = 0; i < grid.length; i++) {
			for (j = 0; j < grid[i].length; j++) {
				Node tempCase = grid[i][j];
				tempCase.render(g);
			}
		}
	}
}