package proto.copy4;

public enum Type {
	Empty, Wall, Start, Target, Explored, Open, Final, Current;
}