package proto.copy4;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

	private Grid grid;
	private Algo algo;

	public KeyInput(Grid grid) {
		this.grid = grid;
		this.algo = new Algo(grid);
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_ESCAPE)
			System.exit(1);

		if (key == KeyEvent.VK_F5)
			this.grid.init(0);

		if (key == KeyEvent.VK_F4)
			this.grid.reset();

//		if (key == KeyEvent.VK_F)
//			this.algo.dijkstra();

		if (key == KeyEvent.VK_B)
			this.algo.bfs();
		
		if (key == KeyEvent.VK_G)
			this.algo.gbfs();

		if (key == KeyEvent.VK_D)
			this.algo.dfs();

		if (key == KeyEvent.VK_A)
			this.algo.astar(true);

		if (key == KeyEvent.VK_Z)
			this.algo.astar(false);

//		if (key == KeyEvent.VK_1)
//			this.grid.test1();

		if (key == KeyEvent.VK_1)
			this.grid.init(1);

		if (key == KeyEvent.VK_2)
			this.grid.init(2);

		if (key == KeyEvent.VK_3)
			this.grid.init(3);

	}

}
